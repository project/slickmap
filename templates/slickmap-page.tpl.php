<html>
  <head>
    <style type="text/css" media="all">
      @import url(<?php print t($variables['slickmap_menus']['css_file']) ?>);
    </style>
  </head>
  <body id="slickmap-page">
    <div class="slickmap-content">
      <?php
      $slickmap_title = check_plain(variable_get('slickmap_title'));
      if (!empty($slickmap_title)) {
        print t('<h1>' . check_plain($slickmap_title) . '</h1>');
      }

      $slickmap_subtitle = variable_get('slickmap_subtitle');
      if (!empty($slickmap_subtitle)) {
        print t('<h2>' . check_plain($slickmap_subtitle) . '</h2>');
      }
      ?>

      <div class="sitemap">
        <?php if (empty($variables['slickmap_menus']['main_menu'])): ?>
          <div class="messages warning">
            <h2 class="element-invisible">Status message</h2>The Slickmap block main menu hasn't been selected yet, please select one on the block configuration settings.
          </div>
        <?php elseif (empty($variables['slickmap_menus']['support_menu'])): ?>
          <div class="messages warning">
            <h2 class="element-invisible">Status message</h2>The Slickmap block support menu hasn't been selected yet, please select one on the block configuration settings.
          </div>
        <?php
        else: ?>


          <ul id="utilityNav">
            <?php foreach ($variables['slickmap_menus']['support_menu'] as $support_menu_link): ?>
              <?php if (isset($support_menu_link['#title'])): ?>
                <li><?php print l(t($support_menu_link['#title']), $support_menu_link['#href'], $support_menu_link['#attributes']) ?></li>
              <?php endif; ?>
            <?php endforeach; ?>
          </ul>

          <?php if ($variables['slickmap_menus']['main_menu_class'] != 1): ?>
            <ul id="primaryNav" class="col<?php print t($variables['slickmap_menus']['main_menu_class']); ?>">
              <?php foreach ($variables['slickmap_menus']['main_menu'] as $main_menu_link): ?>
                <?php if (isset($main_menu_link['link']['link_title']) && $main_menu_link['link']['hidden'] == 0): ?>
                  <li>
                    <?php print l(t($main_menu_link['link']['link_title']), $main_menu_link['link']['link_path']) ?>
                    <?php if (!empty($main_menu_link['below'])): ?>
                      <ul>
                        <?php foreach ($main_menu_link['below'] as $main_menu_second_level): ?>
                          <?php if (isset($main_menu_second_level['link']['link_title']) && $main_menu_second_level['link']['hidden'] == 0): ?>
                            <li>
                              <?php print l(t($main_menu_second_level['link']['link_title']), $main_menu_second_level['link']['link_path']) ?>
                              <?php if (!empty($main_menu_second_level['below'])): ?>
                                <ul>
                                  <?php foreach ($main_menu_second_level['below'] as $main_menu_third_level): ?>
                                    <?php if (isset($main_menu_third_level['link']['link_title']) && $main_menu_third_level['link']['hidden'] == 0): ?>
                                      <li>
                                        <?php print l(t($main_menu_third_level['link']['link_title']), $main_menu_third_level['link']['link_path']) ?>
                                      </li>
                                    <?php endif; //end 3rd level link validation ?>
                                  <?php endforeach; // end 3rd level foreach?>
                                </ul>
                              <?php endif; // end 3rd level validation ?>
                            </li>
                          <?php endif; //end 2nd level link validation ?>
                        <?php endforeach; // end 2nd level foreach?>
                      </ul>
                    <?php endif; // end 2nd level validation ?>
                  </li>
                <?php endif; //end 1st level link validation ?>
              <?php endforeach; // end 1st level foreach ?>
            </ul>


          <?php else: ?>
            <?php foreach ($variables['slickmap_menus']['main_menu'] as $main_menu_link): ?>
              <?php if (count($main_menu_link['below']) > 1): ?>
                <ul id="primaryNav" class="col<?php count($main_menu_link['below']) <= 10 ? print t(count($main_menu_link['below'])) : print t('10'); ?>">
                  <li id="home">
                    <?php print l(t($main_menu_link['link']['link_title']), $main_menu_link['link']['link_path']) ?>
                  </li>
                  <?php foreach ($main_menu_link['below'] as $main_menu_first_level): ?>
                    <?php if (isset($main_menu_first_level['link']['link_title']) && $main_menu_first_level['link']['hidden'] == 0): ?>
                      <li>
                        <?php print l(t($main_menu_first_level['link']['link_title']), $main_menu_first_level['link']['link_path']) ?>
                        <?php if (!empty($main_menu_first_level['below'])): ?>
                          <ul>
                            <?php foreach ($main_menu_first_level['below'] as $main_menu_second_level): ?>
                              <?php if (isset($main_menu_second_level['link']['link_title']) && $main_menu_second_level['link']['hidden'] == 0): ?>
                                <li>
                                  <?php print l(t($main_menu_second_level['link']['link_title']), $main_menu_second_level['link']['link_path']) ?>
                                  <?php if (!empty($main_menu_second_level['below'])): ?>
                                    <ul>
                                      <?php foreach ($main_menu_second_level['below'] as $main_menu_third_level): ?>
                                        <?php if (isset($main_menu_third_level['link']['link_title']) && $main_menu_third_level['link']['hidden'] == 0): ?>
                                          <li>
                                            <?php print l(t($main_menu_third_level['link']['link_title']), $main_menu_third_level['link']['link_path']) ?>
                                          </li>
                                        <?php endif; //end 3rd level link validation ?>
                                      <?php endforeach; // end 3rd level foreach?>
                                    </ul>
                                  <?php endif; // end 3rd level validation ?>
                                </li>
                              <?php endif; //end 2nd level link validation ?>
                            <?php endforeach; // end 2nd level foreach?>
                          </ul>
                        <?php endif; // end 2nd level validation ?>
                      </li>
                    <?php endif; //end 1st level link validation ?>
                  <?php endforeach; // end 1st level foreach ?>
                </ul>
              <?php endif; ?>
            <?php endforeach; ?>
          <?php endif; ?>
        <?php endif; ?>
      </div>
    </div>
  </body>
</html>
