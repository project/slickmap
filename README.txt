
CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------
Slickmap modules provides a quick way to visualize Drupal sitemap in 
Information Architecture phases.


 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/10is8/2460303


 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2460303?categories=All

REQUIREMENTS
------------
No special requirements. 

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * Go to Configuration > Slickmap.  


   - Select main menu and utility menu, click Save.  


   - Navigate to /slickmap to view the rendered map. 


   - Now you can re-order menu and refresh the /slickmap page to see updates. 

TROUBLESHOOTING
---------------
 * If the slickmap is not showing:


   - Have you selected correct menu's? 


   - Do menu's have items?


MAINTAINERS
-----------
Current maintainers:
 * Pedja Grujic (pgrujic) - https://www.drupal.org/u/pgrujic
 * Alvaro Mena (mimodsg) - https://www.drupal.org/u/mimodsg