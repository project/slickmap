<?php

/**
 * @file
 * Administration page callbacks for the Slickmap.
 */

/**
 * Configure Slickmap block information.
 *
 * @ingroup forms
 *
 * @see system_settings_form()
 */
function slickmap_config_form() {
  $form = array();
  $form['menus_configuration'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slickmap menu configurations'),
    '#description' => t('Select the menu that its going to be used for the Slickmap functionality, and the one that its going to be as a support menu'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $slickmap_title = variable_get('slickmap_title');
  $form['menus_configuration']['slickmap_title'] = array(
    '#type' => 'textfield',
    '#title' => 'Slickmap title',
    '#default_value' => !empty($slickmap_title) ? $slickmap_title : '',
  );
  $slickmap_subtitle = variable_get('slickmap_subtitle');
  $form['menus_configuration']['slickmap_subtitle'] = array(
    '#type' => 'textfield',
    '#title' => 'Slickmap subtitle',
    '#default_value' => !empty($slickmap_subtitle) ? $slickmap_subtitle : '',
  );
  $principal_menu = variable_get('slickmap_main_menu');
  $form['menus_configuration']['slickmap_main_menu'] = array(
    '#type' => 'radios',
    '#title' => 'Slick menu',
    '#default_value' => !empty($principal_menu) ? $principal_menu : '',
    '#options' => menu_get_menus(),
  );
  $support_menu = variable_get('slickmap_support_menu');
  $form['menus_configuration']['slickmap_support_menu'] = array(
    '#type' => 'radios',
    '#title' => 'Support menu',
    '#default_value' => !empty($support_menu) ? $support_menu : '',
    '#options' => menu_get_menus(),
  );
  return system_settings_form($form, TRUE);
}
